from flask import Flask, Response, request
import pymongo
import json
from bson import ObjectId
app = Flask(__name__)
try:
    
    mongo = pymongo.mongo_client.MongoClient(
        host = "localhost",
        port = 27017,
        serverSelectionTimeoutMS = 1000)
    db = mongo.library
    mongo.server_info()
except:
    print("Error -Csn not connect the server")


@app.route("/books",methods=["POST"])
def create_books():
    try:
        book = {
            "name" : request.form["name"],
            "type":  request.form["type"],
            "category":  request.form["category"],
            "author":request.form["author"]
        }
        dbResponse = db.books.insert_one(book)
        return Response(
            response = json.dumps(
                {"message" : "Book Created",
                "id": f"{dbResponse.inserted_id}"
                }
            ),
            status = 200,
            mimetype = "application/json"
        )
    except Exception as ex:
        print(ex)
        return Response(
            response = json.dumps(
                {"message" : "book not Created"}
            ),
            status = 500,
            mimetype = "application/json"
        )

@app.route("/books",methods = ["GET"])
def get_books():
    try:
        books_data = list(db.books.find())
        for book_data in books_data:
            book_data["_id"] = str(book_data["_id"])
        return  Response(
            response = json.dumps(books_data),
            status = 200,
            mimetype = "application/json"
        )
    except Exception as Ex:
        print("XXXXXXXXXXXXXXXX",Ex)
        return Response(
            response = json.dumps({"message":"Cant read books the data"}),
            status = 500,
            mimetype = "application/json"
        )
@app.route("/books/<id>",methods = ["PATCH"])
def update_book(id):
    try:
        print("xxxxxxxxxxxxxxxxx",request.form)
        dbResponse = db.books.update_one(
                                        {"_id": ObjectId(id) },
                                        {"$set":
                                            {"name":request.form["name"]}}
                                        ) 
        if dbResponse.modified_count == 1:
            return Response(
            response = json.dumps({"message":"book Updated"}),
            status = 200,
            mimetype = "application/json"
        )
        return Response(
            response = json.dumps({"message":"nothing need to update"}),
            status = 200,
            mimetype = "application/json"
        )
    except Exception as ex:
        print("XXXXXXXXXXXXXXXX",ex)
        return Response(
            response = json.dumps({"message":"Cant update the data"}),
            status = 500,
            mimetype = "application/json"
        )
@app.route("/books/<id>",methods = ["DELETE"])
def delete_book_record(id):
    try:
        dbResponse = db.books.delete_one({"_id":ObjectId(id)})
        if dbResponse.deleted_count == 1:
            return Response(
            response = json.dumps({"message":"book deleted","id":f"{id}"}),
            status = 200,
            mimetype = "application/json"
            )
        return Response(
            response = json.dumps({"message":"book not found","id":f"{id}"}),
            status = 200,
            mimetype = "application/json"
        )
    except Exception as ex:
        print("XXXXXXXXXXXXXXXX",ex)
        return Response(
            response = json.dumps({"message":"Cant delete the data"}),
            status = 500,
            mimetype = "application/json"
        )

if __name__=="__main__":
    app.run(port=8080,debug = True)
